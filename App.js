import React from 'react';
import {Provider} from 'react-redux';
import AppNavigation from './src/AppNavigation';
import store from './src/redux';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppNavigation />
      </Provider>
    );
  }
}

export default App;
