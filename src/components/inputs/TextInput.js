import React from 'react';
import {TextInput as RNTextInput, TextInputProps} from 'react-native';

const TextInput = ({onChange, ...rest}: TextInputProps) => {
  return (
    <RNTextInput
      {...rest}
      onChangeText={onChange}
      style={{
        backgroundColor: 'lightgrey',
        borderRadius: 20,
        marginVertical: 5,
        ...rest.style,
      }}
    />
  );
};

export default TextInput;
