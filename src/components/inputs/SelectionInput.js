import React from 'react';
import {
  Modal,
  Text,
  View,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import TextInput from './TextInput';

const DARK_BLUE = '#10203b';

const SelectionInput = ({
  options = ['Piilihan 1', 'Pilihan 2'],
  value,
  onChange,
}) => {
  const [modalVisible, setModalVisible] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const toggleModal = () => setModalVisible(a => !a);

  const filteredOptions = search
    ? options.filter(option =>
        option.toLowerCase().includes(search.toLowerCase()),
      )
    : options;
  return (
    <React.Fragment>
      <TextInput value={value} onTouchStart={toggleModal} editable={true} />
      <Modal
        visible={modalVisible}
        onRequestClose={toggleModal}
        animationType="slide">
        <View style={{flex: 1, backgroundColor: DARK_BLUE}}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 15,
            }}>
            <TextInput
              value={search}
              onChangeText={setSearch}
              placeholder="Search..."
              style={{width: '85%'}}
            />
            <TouchableOpacity onPress={toggleModal} style={{width: '15%'}}>
              <Text style={{color: 'lightgrey', textAlign: 'right'}}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={filteredOptions}
            keyExtractor={(_, i) => String(i)}
            renderItem={({item}) => {
              const handleSelect = () => {
                toggleModal();
                onChange(item);
              };
              return <ListItem label={item} onPress={handleSelect} />;
            }}
          />
        </View>
      </Modal>
    </React.Fragment>
  );
};

export default SelectionInput;

const ListItem = ({label, onPress}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.listItem}>
      <Text style={styles.listItemText}>{label}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  listItem: {
    color: 'white',
    padding: 15,
  },
  listItemText: {
    color: 'white',
    fontSize: 16,
  },
});
