import React from 'react';
import NumberFormat from 'react-number-format';
import TextInput from './TextInput';

const NumberInput = ({
  value,
  onChange,
  thousandSeparator = '.',
  decimalSeparator = ',',
  prefix = null,
  suffix = null,
  ...rest
}) => {
  // console.log("1st:", value);

  // const onChangeText = text => {
  //   // console.log("3rd:", unformat(text));
  //   handleSetValue(name, text);
  // };

  return (
    <NumberFormat
      value={value ? value : null}
      displayType={'text'}
      thousandSeparator={thousandSeparator}
      decimalSeparator={decimalSeparator}
      prefix={prefix}
      suffix={suffix}
      // onValueChange={values => {
      //   const {formattedValue, value} = values;
      //   onChangeText(value);
      // }}
      renderText={text => {
        // console.log("2nd:", text);
        return (
          <TextInput
            {...rest}
            value={text}
            onChangeText={onChange}
            keyboardType="numeric"
          />
        );
      }}
    />
  );
};

export default NumberInput;
