import React from 'react';
import {
  Image,
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ActivityIndicator,
} from 'react-native';
import {useSelector} from 'react-redux';

const MotorDetailScreen = ({navigation}) => {
  const id = navigation.getParam('id');
  const motor = useSelector(state =>
    state.motor.motors.find(motor => motor.id === id),
  );
  const [loading, setLoading] = React.useState(false);
  React.useEffect(() => {
    const timeoutId = setTimeout(() => setLoading(false), 2000);
    return () => clearTimeout(timeoutId);
  }, []);
  if (loading) {
    return <MotorDetailLoader />;
  }
  return <MotorDetailComponent {...motor} />;
};

export default MotorDetailScreen;

const MotorDetailComponent = ({
  uri,
  brand,
  model,
  price,
  year,
  location,
  transmission,
  usedKilometers,
  phone,
}) => {
  const handleOpenDialer = () => {
    Linking.openURL(`tel:${phone}`);
  };
  const handleOpenWhatsapp = () => {
    Linking.openURL(`whatsapp://send?text=hello&phone=${phone}`);
  };
  return (
    <View style={{flex: 1}}>
      <Image style={{flex: 1, width: null, height: null}} source={{uri}} />
      <View style={{flex: 3}}>
        <View style={styles.box}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <Text style={{fontSize: 22, marginBottom: 5, fontWeight: 'bold'}}>
                {model} {brand}
              </Text>
              <Text style={{fontSize: 16, marginBottom: 5}}>{price}</Text>
              <Text style={{fontSize: 12}}>{year}</Text>
            </View>
          </View>
          <View>
            <Text style={{fontSize: 10}}>{location}</Text>
            <Text style={{fontSize: 10, marginVertical: 5}}>
              {usedKilometers}
            </Text>
            <Text style={{fontSize: 10}}>{transmission}</Text>
          </View>
        </View>
        <TouchableOpacity
          disabled={!phone}
          onPress={handleOpenDialer}
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginVertical: 10,
            ...styles.box,
          }}>
          <Text>Call Gudang Fulan</Text>
          <Text>></Text>
        </TouchableOpacity>
        <TouchableOpacity
          disabled={!phone}
          onPress={handleOpenWhatsapp}
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            ...styles.box,
          }}>
          <Text>WhatsApp Gudang Fulan</Text>
          <Text>></Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const MotorDetailLoader = () => (
  <View style={{...StyleSheet.absoluteFill, justifyContent: 'center'}}>
    <ActivityIndicator size="large" />
  </View>
);

const styles = StyleSheet.create({
  box: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20,
    elevation: 3,
    shadowColor: 'lightgrey',
    shadowOffset: {x: 0, y: 0},
    shadowOpacity: 0.7,
    shadowRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
