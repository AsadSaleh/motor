import React from 'react';
import Form from './components/Form';

const CreateMotorScreen = ({navigation}) => {
  const id = navigation.getParam('id');
  if (id) return <Form edit={true} />;
  return <Form />;
};

export default CreateMotorScreen;
