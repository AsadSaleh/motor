import { Formik } from 'formik';
import React from 'react';
import { ScrollView, StyleSheet, Text, TouchableOpacity } from 'react-native';
import NumberInput from '../../../components/inputs/NumberInput';
import TextInput from '../../../components/inputs/TextInput';
import Colors from '../../../constants/Colors';

const Form = () => {
  const createNewItem = (values: formCreate): void => {
    console.log('values: ', values)
  }
  return (
    <Formik initialValues={initialValues} onSubmit={createNewItem}>
      {({ values, setFieldValue, handleSubmit }) => {
        return (
          <ScrollView contentContainerStyle={{ paddingHorizontal: 15 }}>
            <Text style={styles.formHeader}>Create new</Text>
            <TextInput placeholder="Merek / type" />
            <NumberInput placeholder="CC" value={values.cc} onChange={v => setFieldValue("cc", v)} />
            <NumberInput placeholder="Tahun Kendaraan" value={values.year} onChange={v => setFieldValue("year", v)} />
            <NumberInput placeholder="Jarak Tempuh" value={values.distanceTravelled} onChange={v => setFieldValue("distanceTravelled", v)} />
            <TextInput placeholder="Pajak" value={values.pajak} onChange={v => setFieldValue("pajak", v)} />
            <TextInput placeholder="STNK" value={values.stnk} onChange={v => setFieldValue("stnk", v)} />
            <TextInput placeholder="Kondisi Mesin" value={values.kondisiMesin} onChange={v => setFieldValue("kondisiMesin", v)} />
            <TextInput placeholder="Kondisi Body" value={values.kondisiBody} onChange={v => setFieldValue("kondisiBody", v)} />
            <NumberInput placeholder="Keorisinilan" value={values.orisinalitas} onChange={v => setFieldValue("orisinalitas", v)} />
            <TextInput placeholder="Warna" value={values.warna} onChange={v => setFieldValue("warna", v)} />
            <NumberInput placeholder="Harga" prefix="Rp" value={values.harga} onChange={v => setFieldValue("harga", v)} />
            <TextInput placeholder="Keterangan" value={values.note} onChange={v => setFieldValue("note", v)} />

            <TouchableOpacity style={styles.submitButton} onPress={handleSubmit}>
              <Text style={styles.submitText}>Submit</Text>
            </TouchableOpacity>
          </ScrollView>
        );
      }}
    </Formik>
  );
};

export default Form;

const initialValues = {
  model: '',
  cc: 0,
  year: 0,
  distanceTravelled: 0,
  pajak: false,
  stnk: false,
  kondisiMesin: '',
  kondisiBody: '',
  orisinalitas: 0,
  warna: '',
  harga: 0,
  note: '',
}

type formCreate = typeof initialValues

const styles = StyleSheet.create({
  submitButton: {
    backgroundColor: Colors.blue,
    height: 60,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    marginVertical: 20,
  },
  submitText: { color: 'white', fontSize: 22 },
  formHeader: {
    color: Colors.blue,
    fontSize: 22,
    fontWeight: 'bold',
    marginVertical: 10,
  },
});
