import React, {useState} from 'react';
import {
  Dimensions,
  FlatList,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import BottomSheet from 'reanimated-bottom-sheet';
import FilterItems from './components/FilterItems';
import MotorListItem from './components/MotorListItem';
import Colors from '../../constants/Colors';

const MAX_SHEET_HEIGHT = Dimensions.get('window').height * 0.7;
const HEADER_HEIGHT = MAX_SHEET_HEIGHT * 0.2;
const CONTENT_HEIGHT = MAX_SHEET_HEIGHT * 0.8;

const MotorListScreen = ({navigation}) => {
  const bottomSheet = React.createRef(null);
  const [model, setModel] = useState('');
  const [brand, setBrand] = useState('');
  const [year, setYear] = useState('');
  const openFilter = () => bottomSheet.current.snapTo(1);
  const closeFilter = () => bottomSheet.current.snapTo(0);
  const handlePress = id => {
    navigation.navigate('MotorDetail', {id});
  };
  const motors = useSelector(state => state.motor.motors);
  const filter = {
    ...(model && {model}),
    ...(brand && {brand}),
    ...(year && {year}),
  };

  const handleCreatePress = () => navigation.navigate('CreateMotor');
  const handleResetFilter = () => {
    setModel('');
    setBrand('');
    setYear('');
    closeFilter();
  };

  const filteredData = motors.filter(function(item) {
    for (var key in filter) {
      if (
        item[key] === undefined ||
        item[key].toUpperCase() != filter[key].toUpperCase()
      )
        return false;
    }
    return true;
  });

  return (
    <View style={{backgroundColor: '#f2f9fb', flex: 1}}>
      <FlatList
        data={filteredData}
        keyExtractor={item => item.id + item.model}
        renderItem={({item}) => (
          <MotorListItem {...item} onPress={handlePress} />
        )}
        onScrollBeginDrag={() => {
          closeFilter();
        }}
        ItemSeparatorComponent={() => <View style={{height: 20}} />}
        ListFooterComponent={() => <View style={{height: 30}} />}
        ListHeaderComponent={() => (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              margin: 15,
            }}>
            <Text
              style={{fontSize: 24, fontWeight: 'bold', color: Colors.blue}}>
              Motor
            </Text>
            <View style={{flexDirection: 'row'}}>
              <TouchableOpacity
                onPress={handleCreatePress}
                style={styles.createButtonStyle}>
                <Text
                  style={{color: Colors.blue, fontSize: 22, fontWeight: '600'}}>
                  +
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={openFilter}
                style={styles.filterButtonStyle}>
                <Text style={{color: 'white'}}>Filter</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        contentContainerStyle={{paddingHorizontal: 15}}
      />

      <BottomSheet
        ref={bottomSheet}
        snapPoints={[0, MAX_SHEET_HEIGHT]}
        enabledContentGestureInteraction={false}
        enabledInnerScrolling={false}
        enabledContentTapInteraction={false}
        renderHeader={() => (
          <View
            style={{
              height: HEADER_HEIGHT,
              justifyContent: 'space-between',
              flexDirection: 'row',
              backgroundColor: Colors.blue,
              paddingVertical: 20,
              paddingHorizontal: 20,
            }}>
            <Text style={{fontSize: 32, color: 'white'}}>Filter</Text>
            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <TouchableOpacity onPress={handleResetFilter}>
                <Text style={styles.clearAllText}>Clear All</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={closeFilter}>
                <Text style={styles.saveText}>Save</Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
        renderContent={() => (
          <View style={styles.panel}>
            <FilterItems
              title="Type"
              onPress={setModel}
              selected={model}
              options={models}
            />
            <FilterItems
              title="Brand"
              onPress={setBrand}
              selected={brand}
              options={brands}
            />
            <FilterItems
              title="Year"
              onPress={setYear}
              selected={year}
              options={years}
            />
          </View>
        )}
        initialSnap={0}
      />
    </View>
  );
};

export default MotorListScreen;

const years = [
  '< 2000',
  '2000-2005',
  '2005-2010',
  '2010-2015',
  '2016',
  '2017',
  '2018',
  '2019',
];

const brands = [
  'Honda',
  'Yamaha',
  'Suzuki',
  'Ninja',
  'BMW',
  'Kawasaki',
  'Viar',
  'KTM',
  'TVS',
];

const models = [
  'Nmax',
  'PCX',
  'Beat',
  'Vario',
  'Mio',
  'Scoopy',
  'Vixion',
  'Supra X',
  'Revo',
];

const styles = StyleSheet.create({
  panel: {
    height: CONTENT_HEIGHT,
    backgroundColor: '#10203b',
  },
  filterButtonStyle: {
    backgroundColor: Colors.blue,
    borderRadius: 5,
    width: 80,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  createButtonStyle: {
    backgroundColor: 'white',
    marginRight: 5,
    borderRadius: 5,
    width: 40,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  saveText: {color: 'green', fontSize: 20, fontWeight: 'bold'},
  clearAllText: {
    color: Colors.cinnabarRed,
    fontSize: 16,
    marginRight: 10,
  },
});
