import React from 'react';
import {
  Dimensions,
  FlatList,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';

const FilterItems = ({
  onPress,
  selected,
  options = Array.from({length: 12}).map((a, i) => i),
  title = 'Merek',
}) => {
  return (
    <View style={{paddingVertical: 20}}>
      <View
        style={{
          justifyContent: 'space-between',
          flexDirection: 'row',
          paddingHorizontal: 20,
          marginBottom: 10,
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: 18,
          }}>
          {title}
        </Text>
        {selected ? (
          <Text onPress={() => onPress('')} style={{color: 'grey'}}>
            Clear
          </Text>
        ) : null}
      </View>
      <FlatList
        horizontal
        data={options}
        ListFooterComponent={<View style={{width: 20}} />}
        ListHeaderComponent={<View style={{width: 20}} />}
        ItemSeparatorComponent={() => <View style={{width: 20}} />}
        showsHorizontalScrollIndicator={false}
        keyExtractor={(_, i) => String(i)}
        renderItem={({item, index}) => {
          const isActive = item === selected;
          return (
            <TouchableHighlight onPress={() => onPress(item)}>
              <View
                style={{
                  borderWidth: 0.5,
                  borderRadius: 5,
                  borderColor: 'white',
                  paddingHorizontal: 15,
                  height: 40,
                  alignItems: 'center',
                  justifyContent: 'center',
                  backgroundColor: isActive ? 'white' : 'transparent',
                }}>
                <Text style={{color: isActive ? 'black' : 'white'}}>
                  {item}
                </Text>
              </View>
            </TouchableHighlight>
          );
        }}
      />
    </View>
  );
};

export default FilterItems;
