import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableNativeFeedback,
  View,
} from 'react-native';

const MotorListItem = ({
  uri,
  brand,
  model,
  price,
  year,
  location,
  transmission,
  usedKilometers,
  id,
  onPress,
}) => {
  return (
    <TouchableNativeFeedback
      onPress={() => onPress(id)}
      background={TouchableNativeFeedback.SelectableBackground()}>
      <View style={styles.bro}>
        <Image
          style={{width: 80, height: 50}}
          source={{
            uri,
          }}
        />
        <View style={{}}>
          <Text style={{fontSize: 18}}>
            {brand} {model}
          </Text>
          <Text style={{fontSize: 16}}>{price}</Text>
          <Text style={{fontSize: 12}}>{year}</Text>
        </View>
        <View>
          <Text style={{fontSize: 10}}>{location}</Text>
          <Text style={{fontSize: 10, marginVertical: 5}}>
            {usedKilometers}
          </Text>
          <Text style={{fontSize: 10}}>{transmission}</Text>
        </View>
      </View>
    </TouchableNativeFeedback>
  );
};

export default MotorListItem;

const styles = StyleSheet.create({
  bro: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20,
    elevation: 3,
    shadowColor: 'lightgrey',
    shadowOffset: {x: 0, y: 0},
    shadowOpacity: 0.7,
    shadowRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
