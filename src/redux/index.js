import {combineReducers, createStore, applyMiddleware} from 'redux';
import logger from 'redux-logger';
import auth from './auth';
import motor from './motor';

const appReducer = combineReducers({
  auth,
  motor,
});

const rootReducer = (state, action) => {
  if (action.type === 'UNAUTHORIZED') {
    state = undefined;
  }
  return appReducer(state, action);
};

const store = createStore(rootReducer, applyMiddleware(logger));

export default store;
