const mockupMotors = [
  // nmax
  {
    uri:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS-TWLTU8O2N-r7PVE1hlDrGKhZVr4cvMdEOxWBNPU8q6Cv1Afz',
    model: 'Nmax',
    brand: 'Yamaha',
    price: 25000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //pcx
  {
    uri:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRctawJRzi2s-cScyEhiefjNBB8eFkmcXR6WNeWRYEjVClHlpaD',
    model: 'Pcx',
    brand: 'Honda',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //adv
  {
    uri:
      'https://www.astramotor.co.id/wp-content/uploads/2019/07/X_ADV_ZZ1_MM.png?x95080',
    model: 'Adv',
    brand: 'Honda',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //vario
  {
    uri:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQQPwk_kydB4KciA7yVM9xxJY2DdWWu-OAogwUAyJj2RH4nzb8A',
    model: 'Vario',
    brand: 'Honda',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //ninja
  {
    uri:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRXfq7SVz7JRQRvJJfI8f4LQdEubEu5Z_E1n6PwVwwDTkGFrpFF',
    model: 'Ninja',
    brand: 'Kawasaki',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //mio
  {
    uri:
      'https://www.yamaha-motor.co.id/uploads/products/bYnP9ychMxb5I6wMS1Cm.png',
    model: 'Mio',
    brand: 'Yamaha',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //rxking
  {
    uri:
      'https://d2pa5gi5n2e1an.cloudfront.net/global/images/product/motorcycle/Yamaha_RX_King/Yamaha_RX_King_L_1.jpg',
    model: 'RX-King',
    brand: 'Yamaha',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //beat
  {
    uri: 'https://cdnhonda.azureedge.net/uploads/products/fitures/59638.png',
    model: 'Beat',
    brand: 'Honda',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //vixion
  {
    uri:
      'https://cdn.moladin.com/motor/yamaha/Yamaha_Vixion_All_New_2076_89995_large.jpg',
    model: 'Vixion',
    brand: 'Yamaha',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
  //viar
  {
    uri:
      'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTOevUrsdhSoahWwQW6_dvkM2Wd5QV7gDEfDMn7umSSs0LJ8Jo3',
    model: 'Karya 200',
    brand: 'Viar',
    price: 27000000,
    year: '2019',
    location: 'Gudang A',
    transmission: 'Automatic',
    usedKilometers: 50000,
    phone: '+6287877466867',
    id: Math.floor(100000 + Math.random() * 900000),
  },
];

const init = {
  motors: mockupMotors,
  error: null,
  loading: false,
};

export default function(state = init, action) {
  switch (action.type) {
    case 'LOAD_MOTOR_SUCCESS':
      return {...state, motors: action.payload, loading: false};
    case 'LOAD_MOTOR_ERROR':
      return {...state, error: null, loading: false};
    case 'LOAD_MOTOR_START':
      return {...state, loading: true};
    default:
      return state;
  }
}
