const init = {
  isAuthed: false,
};

export default function(state = init, action) {
  switch (action.type) {
    case 'AUTHORIZED':
      return {...state, isAuthed: true};
    case 'UNAUTHORIZED':
      return {...state, isAuthed: false};
    default:
      return state;
  }
}
