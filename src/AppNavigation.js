import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import MotorList from './screens/MotorList';
import MotorDetail from './screens/MotorDetail';
import CreateMotor from './screens/CreateMotor';

const AppNavigator = createStackNavigator({
  MotorList,
  MotorDetail,
  CreateMotor,
});

export default createAppContainer(AppNavigator);
